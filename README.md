# Python

## 介绍
1. Python是一种计算机程序设计语言。是一种面向对象的动态类型语言，最初被设计用于编写自动化脚本(shell)，随着版本的不断更新和语言新功能的添加，越来越多被用于独立的、大型项目的开发。是一种解释型脚本语言。

### 2. Python基本信息

中文名称:蟒蛇
外文名称:Python
发行时间:1991年
设计者:Guido van Rossum

###3. Python应用领域

Web 和 Internet开发， **软件开发** ，后端开发， **大数据，人工智能** ，科学计算和统计，教育，桌面界面开发

### 4. Python的版本：
本人用的是Linux和windows版本，但是，推荐新人刚开始的时候使用Windows入门，我是以python3.7作为环境使用

### 5. 查看python版本

以Windows为例，其他系统查看的python方法不做赘述，可自行百度一下
```
打开cmd命令行（快捷键：Win + R键 同时按）
输入Python -V，就能返回电脑中安装的python版本。
C:\Users\admin>python -V
Python 3.7.3
```
我的公众号：Java架构师联盟 ，更新内容主要以Java为主，会有一个大数据得体系，不定时更新 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0121/205232_35fb6bd7_7624163.png "屏幕截图.png")



### 6. Python的特点

Python 是一种解释型语言： 这意味着开发过程中没有了编译这个环节。类似于PHP和Perl语言。
Python 是交互式语言： 这意味着，您可以在一个 Python 提示符 >>> 后直接执行代码。
Python 是面向对象语言: 这意味着Python支持面向对象的风格或代码封装在对象的编程技术。
Python 是初学者的语言：Python 对初级程序员而言，是一种伟大的语言，它支持广泛的应用程序开发，从简单的文字处理到 WWW 浏览器再到游戏。
易于学习：Python有相对较少的关键字，结构简单，和一个明确定义的语法，学习起来更加简单。
易于阅读：Python代码定义的更清晰。
易于维护：Python的成功在于它的源代码是相当容易维护的。
一个广泛的标准库：Python的最大的优势之一是丰富的库，跨平台的，在UNIX，Windows和Macintosh兼容很好。
互动模式：互动模式的支持，您可以从终端输入执行代码并获得结果的语言，互动的测试和调试代码片断。
可移植：基于其开放源代码的特性，Python已经被移植（也就是使其工作）到许多平台。
可扩展：如果你需要一段运行很快的关键代码，或者是想要编写一些不愿开放的算法，你可以使用C或C++完成那部分程序，然后从你的Python程序中调用。
数据库：Python提供所有主要的商业数据库的接口。
GUI编程：Python支持GUI可以创建和移植到许多系统调用。
可嵌入: 你可以将Python嵌入到C/C++程序，让你的程序的用户获得"脚本化"的能力。


### 7. Pyhton的第一个简单程序 Hello World

在cmd命令行中，输入python进入python交互界面
输入print('Hello World')，即可领略到python的易上手。
演示样例：

```
C:\Users\admin>python
Python 3.7.3 (default, Mar 27 2019, 17:13:21) [MSC v.1915 64 bit (AMD64)] :: Anaconda, Inc. on win32
Warning:
This Python interpreter is in a conda environment, but the environment has
not been activated. Libraries may fail to load. To activate this environment
please see https://conda.io/activation
Type "help", "copyright", "credits" or "license" for more information.
>>> print('Hello World')
Hello World

```

## 开发工具

Python3.7
PyCharm


## 持续更新

使用说明
因为是刚开始操作，所以有很多不足的地方，大家可以跟我提一下建议
在我的公众号里面，我现在逐步的将文章整理到一些标签中，方便大家成体系得学习 ，嘿嘿嘿
后面我会将公众号中写的比较好得文章每星期更新一次，可以关注公众号：Java架构师联盟，一般都会每天更新一次技术文，哈哈哈哈



