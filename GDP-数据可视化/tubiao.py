#  绘制美国GDP变化tu
import xlrd
# as 起别名
import matplotlib.pyplot as plt
import numpy as np

#1、获取表格中的数据
    #对表格操作  xlrd  xlwt（第三方模块）
    #python 高度近视眼  \t或者 \  t    解决方案：转义字符  \     字符串前面+r
data=xlrd.open_workbook(r'E:\Python\teach\GDP\t4.xlsx')
#print(data.sheet_names())

    #读取表格中的数据
excel=data.sheet_by_name("Sheet")
print(excel.nrows)

#2、确定图表绘制区域

    #设置字体
plt.rcParams['font.sans-serif']=['FangSong']
    #设置字体大小
plt.rcParams.update({'font.size':16})

plt.figure(figsize=(16,9))
plt.title("美国GDP变化图")
    #构建xy轴
plt.grid(linestyle='-.')
plt.xlabel('年份')
plt.ylabel('GDP(万亿)')

#3、填充数据
    #列表   []
year_list=[]
gdp_list=[]
percent_list=[]

#通过for循环遍历每一个单元格中的数据并且进行存储
for i in range(excel.nrows):
    year=excel.cell_value(i,0)
    year_list.append(year)

for i in range(excel.nrows):
    gdp=excel.cell_value(i,1)
    gdp_list.append(gdp)

for i in range(excel.nrows):
    percent=excel.cell_value(i,2)
    percent_list.append(percent)

#!!!!!!!!!!!!重点内容：将数据合并为多源数组
arr=np.array(list(zip(year_list,gdp_list,percent_list)),dtype=np.float)

plt.plot(arr[:,[0]],arr[:,[1]]/1000000000000,"dg",label="GDP")
plt.plot(arr[:,[0]],arr[:,[2]],"--r",label="市场占比")



#4、图标进行展示
plt.legend()
plt.show()